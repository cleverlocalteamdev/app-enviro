<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AppController extends Controller
{
    /**
     * @Route("/", name="dashboard")
     */
    public function dashboardAction(Request $request)
    {
        $data = array('current_route_name' => $request->attributes->get('_route'));
        return $this->render('AppBundle:App:dashboard.html.twig', $data);
    }
     /**
     * @Route("/customer", name="customer")
     */
    public function customerAction(Request $request)
    {
        $data = array('current_route_name' => $request->attributes->get('_route'));
        return $this->render('AppBundle:App:customer.html.twig', $data);
    }

      /**
     * @Route("/customer-request", name="customer-request")
     */
    public function customerRequestAction(Request $request)
    {
        $data = array('current_route_name' => $request->attributes->get('_route'));
        return $this->render('AppBundle:App:customer-request.html.twig', $data);
    }

      /**
     * @Route("/product", name="product")
     */
    public function productAction(Request $request)
    {
        $data = array('current_route_name' => $request->attributes->get('_route'));
        return $this->render('AppBundle:App:product.html.twig', $data);
    }
}
