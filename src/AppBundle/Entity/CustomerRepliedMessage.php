<?php

namespace AppBundle\Entity;

/**
 * CustomerRepliedMessage
 */
class CustomerRepliedMessage
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var \AppBundle\Entity\CustomerRequest
     */
    private $customerRequest;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return CustomerRepliedMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return CustomerRepliedMessage
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set customerRequest
     *
     * @param \AppBundle\Entity\CustomerRequest $customerRequest
     *
     * @return CustomerRepliedMessage
     */
    public function setCustomerRequest(\AppBundle\Entity\CustomerRequest $customerRequest = null)
    {
        $this->customerRequest = $customerRequest;

        return $this;
    }

    /**
     * Get customerRequest
     *
     * @return \AppBundle\Entity\CustomerRequest
     */
    public function getCustomerRequest()
    {
        return $this->customerRequest;
    }
}
