<?php

namespace AppBundle\Entity;

/**
 * CustomerRequest
 */
class CustomerRequest
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var string
     */
    private $details;

    /**
     * @var string
     */
    private $requestCategory;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var integer
     */
    private $hasReplied = 0;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return CustomerRequest
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return CustomerRequest
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set details
     *
     * @param string $details
     *
     * @return CustomerRequest
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set requestCategory
     *
     * @param string $requestCategory
     *
     * @return CustomerRequest
     */
    public function setRequestCategory($requestCategory)
    {
        $this->requestCategory = $requestCategory;

        return $this;
    }

    /**
     * Get requestCategory
     *
     * @return string
     */
    public function getRequestCategory()
    {
        return $this->requestCategory;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return CustomerRequest
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set hasReplied
     *
     * @param integer $hasReplied
     *
     * @return CustomerRequest
     */
    public function setHasReplied($hasReplied)
    {
        $this->hasReplied = $hasReplied;

        return $this;
    }

    /**
     * Get hasReplied
     *
     * @return integer
     */
    public function getHasReplied()
    {
        return $this->hasReplied;
    }
}
